# EmlaLock API

This repository contains code to use the EmlaLock API in different programming languages.

## Contents

- [JavaScript/TypeScript](ts/README.md)
- [.Net](net/README.md)
