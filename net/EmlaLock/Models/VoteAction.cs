﻿namespace EmlaLock.Models
{
    public enum VoteAction
    {
        Subtract = 0,
        Add = 1,
        Random = 2
    }
}