﻿namespace EmlaLock.Models
{
    public enum MessageStatus
    {
        Read = 0,
        Unread = 1,
    }
}