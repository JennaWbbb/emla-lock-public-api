﻿namespace EmlaLock.Models
{
    public enum ChastitySessionType
    {
        Combination = 1,
        Verification = 2
    }
}