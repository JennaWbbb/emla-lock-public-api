﻿using System;

namespace EmlaLock.Services
{
    public interface IEmlaLockConfig
    {
        Uri BaseUrl { get; }
    }
}