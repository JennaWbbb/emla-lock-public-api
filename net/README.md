# EmlaLock API

.Net Standard Wrapper for the [EmlaLock API](https://about.emlalock.com/docs/api)

## Installation

```powershell
Install-Package EmlaLock
```

## Usage

```csharp
using EmlaLock.Services;

// In your entry point, use the helper to register with the dependency injection framework
builder.Services.AddEmlaBot();

// assuming you have an instance from your local friendly dependency injection framework
var info = await _emlaLock.GetInfo(new ApiToken
                {
                    UserId = "abc123",
                    ApiKey = "secret",
                });
```

## License

MIT

