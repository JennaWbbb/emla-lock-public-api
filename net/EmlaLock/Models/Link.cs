﻿using System.Runtime.Serialization;

namespace EmlaLock.Models
{
    public class Link
    {
        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The user.</value>
        [DataMember(Name = "wearer")]
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the chastity session.
        /// </summary>
        /// <value>The chastity session.</value>
        [DataMember(Name = "chastitysession")]
        public ChastitySessionDetails ChastitySession { get; set; }

        /// <summary>
        /// Gets or sets the external link.
        /// </summary>
        /// <value>The external link.</value>
        [DataMember(Name = "externallink")]
        public ExternalLink ExternalLink { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error, or null, if there was no error.</value>
        [DataMember(Name = "error")]
        public string Error { get; set; }
    }
}