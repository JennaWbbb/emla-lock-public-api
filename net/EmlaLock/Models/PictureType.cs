﻿namespace EmlaLock.Models
{
    public enum PictureType
    {
        Combination = 1,
        Verification = 2,
    }
}