﻿using System.Runtime.Serialization;

namespace EmlaLock.Models
{
    [DataContract]
    public class InstaLink
    {
        /// <summary>
        /// Gets or sets the number of seconds this link is set to add.
        /// </summary>
        /// <value>The add.</value>
        [DataMember(Name = "add")]
        public int Add { get; set; }

        /// <summary>
        /// Gets or sets the session.
        /// </summary>
        /// <value>The session.</value>
        [DataMember(Name = "session")]
        public InstaLinkSession Session { get; set; }

        /// <summary>
        /// Gets or sets the usages.
        /// </summary>
        /// <value>The usages.</value>
        [DataMember(Name = "usages")]
        public int Usages { get; set; }

        /// <summary>
        /// Gets or sets the used.
        /// </summary>
        /// <value>The used.</value>
        [DataMember(Name = "used")]
        public int Used { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>The error.</value>
        [DataMember(Name = "error")]
        public string Error { get; set; }
    }
}