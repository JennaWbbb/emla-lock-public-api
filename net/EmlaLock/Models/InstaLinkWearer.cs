﻿using System.Runtime.Serialization;

namespace EmlaLock.Models
{
    [DataContract]
    public class InstaLinkWearer
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        [DataMember(Name = "userid")]
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        [DataMember(Name = "username")]
        public string UserName { get; set; }
    }
}