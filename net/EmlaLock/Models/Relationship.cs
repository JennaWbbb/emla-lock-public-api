﻿namespace EmlaLock.Models
{
    public enum Relationship
    {
        Blocked = -1,
        Pending = 0,
        Accepted = 1,
        None = 2,
    }
}