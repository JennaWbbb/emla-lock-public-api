﻿using System.Runtime.Serialization;

namespace EmlaLock.Models
{
    [DataContract]
    public class InstaLinkSession
    {
        /// <summary>
        /// Gets or sets the wearer.
        /// </summary>
        /// <value>The wearer.</value>
        [DataMember(Name = "wearer")]
        public InstaLinkWearer Wearer { get; set; }

        /// <summary>
        /// Gets or sets the session identifier.
        /// </summary>
        /// <value>The session identifier.</value>
        [DataMember(Name = "sessionid")]
        public string SessionId { get; set; }

        /// <summary>
        /// Gets or sets the session start.
        /// </summary>
        /// <value>The session start.</value>
        [DataMember(Name = "sessionstart")]
        public int SessionStart { get; set; }
    }
}