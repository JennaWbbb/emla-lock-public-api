﻿namespace EmlaLock.Models
{
    public enum LinkProperties
    {
        None = 0,
        Add = 1,
        Subtract = 2,
        Random = 3,
        AddSubtract = 4,
        AddRandom = 5,
        SubtractRandom = 6,
        AddSubtractRandom = 7,
    }
}