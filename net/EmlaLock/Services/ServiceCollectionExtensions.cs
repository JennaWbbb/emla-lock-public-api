﻿using Microsoft.Extensions.DependencyInjection;

namespace EmlaLock.Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEmlaLock(this IServiceCollection services)
        {
            return services
                .AddSingleton<IEmlaLockConfig, EmlaLockConfig>()
                .AddSingleton<IEmlaLock, EmlaLock>();
        }
    }
}