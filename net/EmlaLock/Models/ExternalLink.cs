﻿using System.Runtime.Serialization;

namespace EmlaLock.Models
{
    [DataContract]
    public class ExternalLink
    {
        /// <summary>
        /// Gets or sets the chastity session identifier.
        /// </summary>
        /// <value>The chastity session identifier.</value>
        [DataMember(Name = "chastitysessionid")]
        public string ChastitySessionId { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>The duration.</value>
        [DataMember(Name = "duration")]
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the external link identifier.
        /// </summary>
        /// <value>The external link identifier.</value>
        [DataMember(Name = "externallinkid")]
        public string ExternalLinkId { get; set; }

        /// <summary>
        /// Gets or sets the type of the external link.
        /// </summary>
        /// <value>
        /// The type of the external link, one of:
        /// <list type="table">
        /// <item>requirement</item>
        /// <item>friend</item>
        /// <item>instant</item>
        /// </list>
        /// </value>
        [DataMember(Name = "linktype")]
        public string ExternalLinkType { get; set; }

        /// <summary>
        /// Gets or sets the usages.
        /// </summary>
        /// <value>The usages.</value>
        [DataMember(Name = "usages")]
        public int Usages { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        [DataMember(Name = "userid")]
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the valid until.
        /// </summary>
        /// <value>The valid until.</value>
        [DataMember(Name = "validuntil")]
        public int ValidUntil { get; set; }
    }
}